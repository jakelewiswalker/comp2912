from app import app
from app import db, models
from flask import request, redirect
from sqlalchemy import update
from flask import Flask, render_template, flash
from random import randrange
import tablib
import os
import bcrypt
from .forms import newRegisterForm, loginForm
from flask import Flask, session

@app.route('/')
def index():
    listdb = models.Parent.query.all() #query all users
    listdb2 = models.Child.query.all() #query all locations
    return render_template('index.html',listdb=listdb, listdb2=listdb2, active='index', title='Relational Database Proof Of Architecture')

@app.route('/location')
def location():
    listdb = models.Parent.query.all()
    listdb2 = models.Child.query.all()
    user = listdb[-1] #Get us the current user thats logged in
    coords = request.args.get('coords') #Get the coords through the URL
    loggedin = models.Parent.query.filter_by(id=user.id).first()

    if coords:
        flash('Current location successfully stored in the database')
        valuecoords = models.Child(coords=coords, createdby=user.id) #gets coords from javascript in location page
        db.session.add(valuecoords)
        db.session.commit()
        valuecoords.points.append(user) #When a location is created tell the assoc
        db.session.commit()             #that this user made that record id
        return redirect("/location")

    return render_template('location.html', active='location',  loggedin=loggedin, listdb = listdb, listdb2=listdb2, userstr=str(user.id), userint = user.id ,title='Geolocation Proof Of Architecture')


@app.route('/academic')
def academic():
    dataset = tablib.Dataset()
    with open(os.path.join(os.path.dirname(__file__),'email.csv')) as f:
        dataset.csv = f.read()
    listdb = models.Parent.query.all()
    user = listdb[-1] #Get us the current user thats logged in
    id = str(user.id) #Convert to a string so we can view in the html page
    dataid = dataset['id']
    found=0
    i=0
    while i < len(dataid): #loop through data to say if the user is found in records
        if id == dataid[i]:
            found = 1
        i=i+1
    return render_template('academic.html', active='academic', title='Academic Records Proof Of Architecture', found=found, data=dataset,id=id, user=user)

@app.route('/rawacademic')
def rawacademic():
    dataset = tablib.Dataset() #Convert csv to list
    with open(os.path.join(os.path.dirname(__file__),'email.csv')) as f:
        dataset.csv = f.read()
    return render_template('rawacademic.html', active='rawacademic', title='Raw Academic Records Proof Of Architecture', data=dataset)

@app.route('/login', methods=['GET', 'POST'])
def login():
    listdb = models.Tasks.query.all()
    form2 = newRegisterForm() #Registration form
    if form2.validate_on_submit():
        for d in listdb:
            if d.username == form2.username.data: #Is username is user?
                flash('Please choose another username this is already in use')
                return redirect("/login")
        if form2.password.data != form2.password2.data: #Do the passwords match?
            flash('Passwords do not match, please try again')
            return redirect("/login")
        else:
            flash('Account Created') #Account is good so create account
            value = models.Tasks(username=form2.username.data, password=form2.password.data)
            db.session.add(value)
            db.session.commit()
            return redirect("/login")


    form = loginForm() #Login form
    username = 0;
    if 'username' in session:
        username = session['username'] #if a session is set pass that to page

    if form.validate_on_submit():
          for d in listdb:
              if (form.loginuser.data == d.username) and (form.loginpass.data == d.password):
                  session['username'] = d.username #Creating a session for the username
                  flash('Successfully logged in')
                  return redirect("/login")
              else:
                  flash('Incorrect Username and password combination')
                  return redirect("/login")

    passwd = b'example-password' #use bcrypt to encrypt a string
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(passwd, salt)

    return render_template('login.html', active='login', title='Login Proof Of Architecture', username=username, passwd=passwd, salt=salt, hashed=hashed,form=form,form2=form2)

@app.route('/newuser')
def newuser():
    user = models.Parent(username=randrange(1000), password=randrange(1000)) #Randomly generates user name and password
    db.session.add(user)
    db.session.commit()
    return redirect("/location")
    return ("New user created")

@app.route('/dropuser')
def dropuser():
    models.Parent.query.delete()
    user = models.Parent(username=randrange(1000), password=randrange(1000)) #Randomly generates user name and password
    db.session.add(user)
    db.session.commit()
    return redirect("../")

@app.route('/droploc')
def droploc():
    models.Child.query.delete() #Deletes table of locations
    db.session.commit()
    return redirect("../")

@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect("login")
