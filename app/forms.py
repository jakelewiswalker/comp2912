from flask_wtf import Form
from wtforms import IntegerField, StringField
from wtforms.validators import DataRequired

class newRegisterForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    password2 = StringField('password2', validators=[DataRequired()])

class loginForm(Form):
    loginuser = StringField('loginuser', validators=[DataRequired()])
    loginpass = StringField('loginpass', validators=[DataRequired()])
