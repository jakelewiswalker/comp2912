from app import db

association_table = db.Table('association', db.Model.metadata,
    db.Column('left_id', db.Integer, db.ForeignKey('left.id')),
    db.Column('right_id', db.Integer, db.ForeignKey('right.id'))
)

class Parent(db.Model):
    __tablename__ = 'left'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(500), index=True)
    password = db.Column(db.String(500), index=True)
    interests = db.relationship("Child", secondary=association_table, backref=db.backref('points', lazy='dynamic'))

class Child(db.Model):
    __tablename__ = 'right'
    id = db.Column(db.Integer, primary_key=True)
    createdby = db.Column(db.Integer)
    coords = db.Column(db.String(500))

class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(500))
    password = db.Column(db.String(500))
