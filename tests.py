import os
import unittest
from config import basedir
from app import app, db, models
from flask import Flask, render_template, flash, session, request, redirect
from sqlalchemy import update

# The tests cover all boundary cases

class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def register(self, username, password, password2):
        return self.app.post(
            '/login',
            data=dict(username=username, password=password, password2=password2),
            follow_redirects=True
        )

    def login(self, username, password):
        return self.app.post(
            '/login',
            data=dict(loginuser=username, loginpass=password),
            follow_redirects=True
        )

    def logout(self): #Test logout
        return self.app.get('/logout',follow_redirects=True)

    ##########REGISTER TEST
    def test_register_page(self):
            response = self.app.get('/login', follow_redirects=True) #Ensure register page works
            self.assertEqual(response.status_code, 200)

    def test_valid_user_registration(self):
        response = self.register('jakewalker', 'flask', 'flask') #Create Account
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Account Created', response.data)

    def test_different_password_registration(self):
        response = self.register('jakewalker123', 'flask', 'flask2') #Create not matching passwords account
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Passwords do not match, please try again', response.data)

    def test_already_made_registration(self):
        response = self.register('jakewalker', 'flask', 'flask') #Create duplicate username
        response = self.register('jakewalker', 'flask', 'flask')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Please choose another username this is already in use', response.data)

    def test_valid_login_valid(self):
        self.register('jakewalker', 'flask', 'flask') #Create Account
        response = self.login('jakewalker', 'flask') #Login Account with correct username / pwd
        self.assertIn(b'Successfully logged in', response.data)

    # def test_valid_login_incorrect(self):
    #     response = self.login('randomusername123', 'abcdefg') #Login Account with invalid data
    #     self.assertEqual(response.status_code, 200)
    #     self.assertIn(b'Incorrect Username and password combination', response.data)


if __name__ == '__main__':
    unittest.main()
